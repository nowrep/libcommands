#include "register_cmds.h"
#include <console/console.h>

#include <console/utils.h>
#include <pld/conf_payloads.h>

#if PLD_HAVE_EXPORT
#include <cJSON.h> // exported by the payloads library
#endif

void register_cmds()
{
    #if PLD_HAVE_EXPORT
        // Ensure cJSON and the pretty-print module use the console allocator
        cJSON_Hooks hooks = {
            .free_fn = console_free,
            .malloc_fn = console_malloc,
        };
        cJSON_InitHooks(&hooks);
    #endif

    extern int cmd_txn_conf(console_ctx_t *ctx, cmd_signature_t *reg);
    console_cmd_register(cmd_txn_conf, "txnstyle");
}
